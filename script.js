document.addEventListener("DOMContentLoaded", async function() {
	const response = await fetch(
		"https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/global-temperature.json"
	);
	const data = await response.json();
	const dataset = data.monthlyVariance;
	console.log(data);

	const width = 1400;
	const height = 500;
	const padding = 60;

	const heightOfBar = (height - padding) / 12;

	const svg = d3
		.select(".container")
		.append("svg")
		.attr("class", "svg")
		.attr("width", width)
		.attr("height", height);

	const minYear = d3.min(dataset, d => d.year);
	const maxYear = d3.max(dataset, d => d.year);

	const xScale = d3
		.scaleTime()
		.domain([new Date(minYear, 0, 0), new Date(maxYear, 0, 0)])
		.range([padding, width - padding]);

	const yScale = d3
		.scaleTime()
		.domain([new Date(0, 12, 0), new Date(0, 0, 0)])
		.range([height - padding, 0]);

	const xAxis = d3.axisBottom(xScale).tickArguments([d3.timeYear.every(10)]);
	const yAxis = d3.axisLeft(yScale).tickFormat(d3.timeFormat("%B"));

	const getColor = t => {
		if (t < 2.8) return "rgb(49, 54, 149)";
		if (t < 3.9 && t >= 2.8) return "rgb(69, 117, 180)";
		if (t < 5.0 && t >= 3.9) return "rgb(116, 173, 209)";
		if (t < 6.1 && t >= 5.0) return "rgb(171, 217, 233)";
		if (t < 7.2 && t >= 6.1) return "rgb(224, 243, 248)";
		if (t < 8.3 && t >= 7.2) return "rgb(255, 255, 191)";
		if (t < 9.5 && t >= 8.3) return "rgb(254, 224, 144)";
		if (t < 10.6 && t >= 9.5) return "rgb(253, 174, 97)";
		if (t < 11.7 && t >= 10.6) return "rgb(244, 109, 67)";
		if (t < 12.8 && t >= 11.7) return "rgb(215, 48, 39)";
		return "rgb(165, 0, 38)";
	};

	const colors = [
		"rgb(49, 54, 149)",
		"rgb(69, 117, 180)",
		"rgb(116, 173, 209)",
		"rgb(171, 217, 233)",
		"rgb(224, 243, 248)",
		"rgb(255, 255, 191)",
		"rgb(254, 224, 144)",
		"rgb(253, 174, 97)",
		"rgb(244, 109, 67)",
		"rgb(215, 48, 39)",
		"rgb(165, 0, 38)"
	];

	svg.selectAll("rect")
		.data(dataset)
		.enter()
		.append("rect")
		.attr("class", "cell")
		.attr("x", (d, i) => xScale(new Date(d.year, 0)))
		.attr("y", d => yScale(new Date(0, d.month - 1, 0)))
		.attr("width", (width - 2 * padding) / (maxYear - minYear))
		.attr("height", heightOfBar)
		.attr("fill", d => getColor(data.baseTemperature + d.variance))
		.attr("data-month", d => d.month - 1)
		.attr("data-year", d => d.year)
		.attr("data-temp", d => data.baseTemperature + d.variance);

	svg.append("g")
		.attr("id", "x-axis")
		.attr("transform", `translate(${0}, ${height - padding})`)
		.call(xAxis);

	svg.append("g")
		.attr("id", "y-axis")
		.attr("transform", `translate(${padding}, 0)`)
		.call(yAxis)
		.selectAll(".tick")
		.attr(
			"transform",
			d => `translate(0, ${d.getMonth() * heightOfBar + heightOfBar / 2})`
		);

	const legend = svg
		.append("g")
		.attr("id", "legend")
		.attr("transform", `translate(${padding}, ${height - padding / 2})`);

	legend
		.selectAll("rect")
		.data(colors)
		.enter()
		.append("rect")
		.attr("width", 25)
		.attr("height", 25)
		.attr("fill", d => d)
		.attr("x", (d, i) => 25 * i);
/*
	const legendScale = d3.scaleLinear()
			.domain([0, 13])
			.range([0, 25 * colors.length]);
	const legendAxis = d3.axisBottom(legendScale);
	legend.call(legendAxis)
*/
	const cells = document.querySelectorAll(".cell");
	const tooltip = document.querySelector("#tooltip");
	cells.forEach(cell => {
		cell.onmouseover = e => {
			const { month, year, temp } = cell.dataset;
			const x = cell.getAttribute("x");
			const y = cell.getAttribute("y");
			tooltip.setAttribute("data-year", year);
			tooltip.innerHTML = `<p>Year: ${year}</p><p>Month: ${month}</p><p>${Math.round(
				temp * 10
			) / 10}°C</p>`;
			tooltip.classList.add("tooltip-show");
			cell.classList.add("stroke");
			tooltip.style.left = x - 35 + "px";
			tooltip.style.top = y + "px";
		};
		cell.onmouseout = () => {
			tooltip.classList.remove("tooltip-show");
			cell.classList.remove("stroke");
		};
	});
});
